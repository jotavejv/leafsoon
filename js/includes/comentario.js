$('.comentarioValue').on('focus', function(){
    $('.enviarComentario').addClass('active')
});
$('.comentarioValue').on('blur', function(){
    setTimeout(function() {
        $('.enviarComentario').removeClass('active');
    }, 500);
});
$('.enviarComentario').one('click', function() {
    let val = $(this).closest('.comentarios').find('.comentarioValue').val();
    let n = new Date();
    let hours = n.getHours();
    let minutes = n.getMinutes();
    $(this).closest('.comentarios').find('#hours').text(hours);
    if(minutes < 10){
        minutes = "0"+minutes;
    }
    $(this).closest('.comentarios').find('#minutes').text(minutes);
    $(this).closest('.comentarios').find('#comentario').text(val);
    $(this).closest('.comentarios').find('.comentarioValue').val('');
    $(this).closest('.comentarios').find('.nextComentario').addClass('active');
});
