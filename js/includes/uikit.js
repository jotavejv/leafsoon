//:menu
    $('.menu').on('click', () => {
        $('body').toggleClass('menu-active');
    });
//:

//:header
var $prevScrollTop = 0;
$(window).scroll(function(){
    var $target = $('.header');
    var $currentScrollTop = $(window).scrollTop();
    if($currentScrollTop > $prevScrollTop){
        $target.removeClass('active');
        $prevScrollTop = $currentScrollTop;
    }else{
        $target.addClass('active');
        $prevScrollTop = $currentScrollTop - 1;
    }
});
//:

//: scrollTo
$("a").on('click', function(e) {
    e.stopPropagation();
    if($('body').hasClass('menu-active')){
        $('body').toggleClass('menu-active');
    }
    var anchor = $(this).prop('href');
    var target = $(this.hash);

    target = target.length ? target : '';
    if (target.length) {

    $('html,body').animate({
          scrollTop: target.offset().top
        }, 500, function(){
          window.location.href = anchor;
        });
        return false;
    }
});
//:
