function getScript(path){
    return new Promise((resolve, reject) => {
        $.getScript(path + '.js')
        .done(function(script){
            console.log("script loaded");
            resolve(script);
        })
    });
}
