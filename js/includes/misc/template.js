var Template = (function(){

    function async(template, target) {
      return new Promise((resolve, reject) => {
        var element = "[data-template=" + target + "]";
        var html = template + '.html';
        $.get( html, function( data ) {
          $(element).html(data);
          resolve(data);
        });
      });
    };

    return {
        get: async,
        destroy: function(target){
            var element = "[data-template=" + target + "]";
            $(element).html('');
        }
    }
})();
