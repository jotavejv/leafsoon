//:divider
    const sectionDivider = $(".section-divider");
    const sectionDividerActive = $(".section-divider.is-active");
    if(sectionDivider){
        sectionDivider.on("click", ".section-divider-title", function(e) {
            $(this)
                .closest(".section-divider")
                .toggleClass('is-active')
                .find('.section-divider-content')
                .slideToggle('fast');
        });
    }
    if(sectionDividerActive) sectionDividerActive.find('.section-divider-content').show(); // verifica uma unica vez para os itens que devem estar abertos por default
//:
