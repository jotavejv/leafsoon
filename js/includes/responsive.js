module.exports = function(breakpoint, add, remove){
    window.matchMedia(breakpoint)
        .addListener(function(mql) {
            if (mql.matches) {
                if(add) add()
            } else {
                if(remove) remove()
            }
        });
}
