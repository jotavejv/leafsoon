let selectMultiple =$('.ui-select--multiple');
if(selectMultiple){
    selectMultiple.each(function(index, el) {
        let selectMultipleInput = $(el).find('input');
        let selectOptionsMultiple = $(el).find('.ui-select-options .ui-checkbox');
        let selectMultipleValues = [];
        selectOptionsMultiple.each(function(i, option) {
            $(option).on('click', function(e) {
                e.preventDefault();
                e.stopPropagation();
                let check = $(option).find('input').prop('checked');
                $(option).find('input').attr('checked', !check);
                let textVal = $(option).find('.check-option').text();
                if($(option).find('input').prop('checked')){
                    selectMultipleValues.push(textVal);
                }else{
                    let index = selectMultipleValues.indexOf(textVal);
                    selectMultipleValues.splice(index, 1);
                }
                selectMultipleInput.val(selectMultipleValues.join(', '))
            });
        });
    });
}
