let select =$('.ui-select');
if(select){
    let selectInput = select.find('input');
    select.each(function(index, el){
        $(el).find('input').on('focus', function(){
            $(el).closest('.ui-select').addClass('select-active');
            if($(el).hasClass('ui-select--multiple')) handleClickDropDownsOpen();
        });
        if($(el).hasClass('ui-select--multiple')) return;
        $(el).find('.ui-select-options li').on('click', function(e) {
            $(el).find('.input-select').val($(this).text());
        });
        $(el).find('input').on('blur', function(){
            setTimeout(function () {
                $(el).closest('.ui-select').removeClass('select-active');
            }, 200);
        });
    });
}
