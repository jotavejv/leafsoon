let panelCollapse = $(".ui-panel--collapse");
let panelCollapseActive = $(".ui-panel--collapse.panel-active");

if(panelCollapse){
    panelCollapse.off("click").on("click", ".ui-panel-header", function(e) {
        let $check =  $(this).find('.ui-checkbox input');
        $(this).find('.ui-label').fadeIn('fast');
        if($check.prop('checked')){
            $check.removeAttr('checked');
            $(this).find('.edit-panel').removeClass('active');
        }
        $(this)
            .closest(".ui-panel--collapse")
            .toggleClass('panel-active')
            .find('.ui-panel--collapse-content')
            .slideToggle('fast');
    });
}
if(panelCollapseActive) panelCollapseActive.find('.ui-panel--collapse-content').show();
