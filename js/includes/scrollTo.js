$("a.toTop").on('click', function(e) {
    e.stopPropagation();
    var anchor = $(this).prop('href');
    var target = $(this.hash);
    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
    if (target.length) {

    $('html,body').animate({
          scrollTop: target.offset().top
        }, 500, function(){
          window.location.href = anchor;
        });
        return false;
    }

});
