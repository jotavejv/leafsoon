let spinInput = $('.spin-number');
if(spinInput){
    let handleInterval;
    spinInput.each(function(index, el) {
        $(el).closest('.spin-control').find('.spin-up').on('mousedown', function(e){
            clearInterval(handleInterval);
            handleInterval = setInterval(function(){
                spinUpFn($(el));
            }, 50);
        })
        $(el).closest('.spin-control').find('.spin-down').on('mousedown', function(e){
            clearInterval(handleInterval);
            handleInterval = setInterval(function(){
                spinDownFn($(el));
            }, 50);
        })
        $(el).closest('.spin-control').find('.spin-up').on('mouseup', function(e){
            clearInterval(handleInterval);
        })
        $(el).closest('.spin-control').find('.spin-down').on('mouseup', function(e){
            clearInterval(handleInterval);
        })
    });
    function handleChangeSpinNumber(val, element){
        let $element = element;
        let $val = val;
        if($val < 0) $element.val(0) // nao permite numericos negativos
    }
    let currentValue; // valor do input do exemplo.
    function spinUpFn(el){
        currentValue = el.val();
        currentValue++;
        el.val(currentValue);
        handleChangeSpinNumber(currentValue, el);
    }
    function spinDownFn(el){
        currentValue = el.val();
        currentValue--;
        el.val(currentValue);
        handleChangeSpinNumber(currentValue, el);
    }
}
