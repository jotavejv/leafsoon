//:slider
    function sliderIputVal(val, element){
        element.closest('.ui-slider-input').querySelector('.ui-slider-input-value').innerHTML = val;
    }
    if(Array.from(document.querySelectorAll('.ui-slider-input'))){
        Array.from(document.querySelectorAll('.ui-slider-input')).forEach(function (slider){
            let initialValue = slider.querySelector('input').value;
            slider.querySelector('.ui-slider-input-value').innerHTML = initialValue;

            slider.querySelector('input').addEventListener('input', function(e){
                sliderIputVal(e.target.value, e.target);
            });
        });
    }
//:
