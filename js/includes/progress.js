//: progressbar
    let progressValue = $('.ui-progress-bar');
    if(progressValue){
        progressValue.each(function(index, el) {
            let val = $(el).text();
            $(el).css('width', val);
        });
    }
//:
