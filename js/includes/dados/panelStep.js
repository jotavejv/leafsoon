$('.panelStep').on('click', function() {

    if( $(this).hasClass('panelStepPercent') ){
        if($('#sliderPercent input').val() == 0){
            $('#sliderPercent').addClass('ui-error');
            return;
        }else{
            $('#sliderPercent').removeClass('ui-error');
        }
    }

    var $panel = $(this).closest('.ui-panel');
    var $nextPanel = $(this).closest('.ui-panel').next('.ui-panel');
    var $checkStep = $panel.find('.ui-panel--collapse-header .ui-checkbox input');
    var checkValidity = $checkStep.prop('checked');
    $panel.removeClass('require');
    $panel.find('.ui-panel--collapse-header').trigger('click');


    if(!$nextPanel.hasClass('panel-active') && !$nextPanel.find('.ui-panel--collapse-header .ui-checkbox input').prop('checked')){
        $nextPanel.find('.ui-panel--collapse-header').trigger('click');
    }

    var panelTop = ($panel.offset().top);

    if(window.innerWidth < 930){
        panelTop = panelTop - 60;
    }else{
        panelTop = panelTop - 20
    }

    $('html,body').animate({
        scrollTop: panelTop
    }, 500);
    setTimeout(function(){
        $checkStep.prop('checked', !checkValidity);
        $panel.find('.ui-panel--collapse-header .ui-label').fadeOut('fast');
        $panel.find('.edit-panel').addClass('active');
    }, 450)
});
