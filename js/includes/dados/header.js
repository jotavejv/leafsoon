$(window).scroll(function(){
    var $target = $('.panel-main-action');
    var $scrollTop = $(window).scrollTop();
    var $targetToShow = $('.panel-sticky');
    if($scrollTop > 200){
        $targetToShow.addClass('active');
    }else{
        $targetToShow.removeClass('active');
    }

    // handle menu anchor
    // if($scrollTop >= panelIdentificacao.offset().top){
    //     console.log($("li[data-target="+ panelIdentificacao.prop('id') +"]"))
    //     $("li[data-target="+ panelIdentificacao.prop('id') +"]").addClass('active');
    // }

    $('li[data-target]').each(function () {
        var currLink = $(this);
        var refElement = $('#' + currLink.data('target'));
        if (refElement.position().top <= $scrollTop) {
            $('li[data-target]').removeClass("active");
            currLink.addClass("active");
        }
        else{
            currLink.removeClass("active");
        }
        if( $scrollTop <= $('.wrapper-collumns .ui-panel').first().offset().top){
            $('li[data-target=' + $('.wrapper-collumns .ui-panel').prop('id') + ']').addClass('active');
        }
        if (document.body.scrollHeight == $scrollTop + window.innerHeight) {
            $('li[data-target]').removeClass("active");
            $('li[data-target=' + $('.wrapper-collumns .ui-panel').last().prop('id') + ']').addClass('active');
        }
    });

});
