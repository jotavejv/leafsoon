function bodyMobile(){
    $('body').toggleClass('body-mobile');
}
$('.triggerMobileInfo').on('click', function() {
    $('.mobile-info').toggleClass('active');
    bodyMobile();
});
$('.mobile-close').on('click', function() {
    $(this).closest('.mobile-menu').removeClass('active');
    bodyMobile();
});

$('.float-button').on('click', function() {
    $(this).toggleClass('active');
    $('.mobile-actions').toggleClass('active');
    bodyMobile();
});
