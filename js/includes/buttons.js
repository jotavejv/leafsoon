//:buttons
    let splitButton = $(".split-button");
    let iconSelector = $(".icon-selector");
    let buttonSelector = $(".button-selector");
    let radioButton = $(".radio-button-element .ui-button");
    let tabButton = $(".tab-button-element");
    let linkSelector = $('.link-selector');
    if(linkSelector)linkSelector.on('click', toggleDropdownActiveClass);
    if(splitButton) splitButton.on('click', toggleDropdownActiveClass);
    if(iconSelector) iconSelector.on('click', toggleDropdownActiveClass);
    if(buttonSelector) buttonSelector.on('click', toggleDropdownActiveClass);
    if(radioButton) radioButton.on('click', toggleRadioButtonActiveClass)
    if(tabButton) tabButton.on('click', toggleTabButtonActiveClass)
    function toggleDropdownActiveClass(e) {
        e.stopPropagation();
        $('.ui-dropdown-options').closest('.active').removeClass('active');
        handleClickDropDownsOpen();
        $(this).toggleClass('active');
    }
    function toggleRadioButtonActiveClass(e) {
        e.stopPropagation();
        radioButton.removeClass('active');
        $(this).addClass('active');
    }
    function toggleTabButtonActiveClass(e) {
        e.stopPropagation();
        tabButton.removeClass('active');
        $(this).addClass('active');
    }
//:
