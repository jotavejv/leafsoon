$('.tiggerPopover').on('click', function(e) {
    e.stopPropagation();
    e.preventDefault();
    handleClickDropDownsOpen();
    var target = $(this).data('popover');
    $(target).toggleClass('active');
});
