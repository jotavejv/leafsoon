require('global')
require('divider')
require('buttons')
require('spinButton')
require('panelCollapse')
require('slider')
require('select')
require('selectMultiple')
require('modal')
require('comentario')
require('onlyNumber')
require('progress')
require('scrollTo')
require('popover')
require('uikit')
// require('dados/mobile')
// require('dados/more')
// DEVCENTER
$('.devcenter .radio-button .ui-button').on('click', function(){
    const size = $(this).find('.fa').attr('class').replace("fa fa-", "");
    let screen = $('main');
    if(size == "desktop"){
        if(window.innerWidth < 1200){
            screen.css({
                "width": "1200px"
            })
        }else{
            screen.css({
                "width": "100%"
            })
        }
    }
    if(size == "tablet"){
        screen.css({
            "width": "800px"
        })
    }
    if(size == "mobile"){
        screen.css({
            "width": "400px"
        })
    }
});
$(function(){
    let responsive = require('responsive');

    responsive('(max-width: 1600px)',
        function(){
            console.log("add")
        }, function(){
            console.log("remove")
        }
    )

    function handleClickDropDownsClose() {
        $('.ui-dropdown-options').closest('.active').removeClass('active');
        $('.ui-select-options').closest('.select-active').removeClass('select-active');
        $('.ui-popover.active').removeClass('active');
        $('.dropdownOverlayClick').removeClass('active');
    }
    $('.dropdownOverlayClick').off('click').on('click', handleClickDropDownsClose);

    //:inputs

        //: counter textarea
            let countDownElement = document.querySelector('.text-countdown');
            let countDownValue = 250; // define o valor do contador
            countDownElement.textContent = countDownValue; // inicia o valor do contador no elemento

            let textArea = document.querySelector('.ui-textarea textarea');
            textArea.addEventListener('input', function(e){
                let valLength = e.target.value.length;
                let computedValue = countDownValue - valLength;
                countDownElement.textContent = computedValue <= 0 ? 0 : computedValue;
                e.target.value = e.target.value.slice(0, countDownValue); // limite de caracteres pro textarea
            });
        //:

        //: zoom-lookup
            let inputSearch = document.getElementById('input-zoom-lookup');
            let searchOptions = document.querySelector('.zoom-lookup-exemplo1 .zoom-lookup-options');
            let searchArray = ['alfredo', 'ana', 'astrogildo', 'afonso', 'anderson', 'alisson', 'atanagildo', 'emerson', 'bruna', 'joao', 'joaquim'];
            let inputSearch_val;
            let autocomplete_results;

            inputSearch.addEventListener('focus', function(){
                $('.zoom-lookup-exemplo1 .zoom-lookup-wrapper').addClass('zoom-lookup-active');
            }, true);
            inputSearch.addEventListener('blur', function(){
                $('.zoom-lookup-exemplo1 .zoom-lookup-wrapper').removeClass('zoom-lookup-active');
            }, true);

            function autocomplete(val) {
              var searchArray_return = [];

              for (var i = 0; i < searchArray.length; i++) {
                if (val === searchArray[i].slice(0, val.length)) {
                  searchArray_return.push(searchArray[i]);
                }
              }

              return searchArray_return;
            }

            function handleActiveSearch(){
                searchOptions.classList.remove('zoom-lookup-active');
            }
            function removeTag(){
                $('.zoom-lookup-exemplo1 .ui-tag').on('click', '.remove-tag', function(){
                    $(this).closest('.ui-tag').remove();
                    if(!$('.zoom-lookup-exemplo1 .ui-tag').length){
                        $('.zoom-lookup-exemplo1 .zoom-lookup-wrapper').removeClass('has-tag');
                    }
                });
                $('.zoom-lookup-exemplo1 .remove-tags').on('click', function(){
                    $('.zoom-lookup-exemplo1 .ui-tag').remove();
                    $('.zoom-lookup-exemplo1 .zoom-lookup-wrapper').removeClass('has-tag');
                });
            }

            function handleTags(val) {
                $('.zoom-lookup-exemplo1 .zoom-lookup-wrapper').addClass('has-tag');
                $(inputSearch).before("<span class='ui-tag'><a href='#'>"+ val +"</a><span class='icon-element remove-tag'><i class='fa fa-times'></i></span></span>");
                inputSearch.value = '';
                removeTag();
            }

            function bindEventsOnList(){
                Array.from(searchOptions.querySelectorAll('li')).forEach(function(option){
                    option.addEventListener('click', function(e){
                        handleActiveSearch();
                        let val = this.textContent;
                        handleTags(val);
                    });
                });
            }

            document.querySelector('.label-myself').addEventListener('click', function(e){
                handleActiveSearch();
                let val = this.querySelector('span').textContent;
                handleTags(val);
            });

            inputSearch.onkeyup = function(e) {
                //has-tag
              inputSearch_val = this.value; // updates the variable on each ocurrence

              if (inputSearch_val.length > 0 && e.keyCode !== 27) {
                searchOptions.classList.add('zoom-lookup-active');
                var searchArray_to_show = [];

                autocomplete_results = document.getElementById("js-zoom-lookup-options");
                autocomplete_results.innerHTML = '';
                searchArray_to_show = autocomplete(inputSearch_val);

                for (var i = 0; i < searchArray_to_show.length; i++) {
                  autocomplete_results.innerHTML += '<li>' + searchArray_to_show[i] + '</li>';
                }
                if(!searchArray_to_show.length){
                    $('.zoom-lookup-exemplo1 .zoom-lookup-create').addClass('show-create');
                    $('.zoom-lookup-exemplo1 .create-zoom').text(inputSearch_val);
                    $('.zoom-lookup-exemplo1 .list-more').removeClass('show-list-more');
                }else{
                    $('.zoom-lookup-exemplo1 .zoom-lookup-create').removeClass('show-create');
                }
                if(searchArray_to_show.length > 5){
                    $('.zoom-lookup-exemplo1 .list-more').addClass('show-list-more');
                }else{
                    $('.zoom-lookup-exemplo1 .list-more').removeClass('show-list-more');
                }
                bindEventsOnList();
              } else {
                inputSearch.value= '';
                handleActiveSearch();
                searchArray_to_show = [];
                autocomplete_results.innerHTML = '';
              }
            }
        //:

        /*
        * advanced search
        */

        //:exemplo search default*/
            //select
            let selectAdvanced = document.querySelector('.ui-select-exemplo2.ui-select input');
            let selectOptionsAdvanced = document.querySelectorAll('.ui-select-exemplo2 .ui-select-options li');
            Array.from(selectOptionsAdvanced).forEach(function(option){
                option.addEventListener('click', function(e) {
                    $('.advanced-search1 .zoom-lookup').removeClass('is-disabled');
                    $('.advanced-search1 .ui-button').removeClass('is-disable');
                    $('.advanced-search1 .zoom-lookup input').trigger('focus');
                    let val = e.target.textContent;
                    selectAdvanced.value = val;
                });
            });
            selectAdvanced.addEventListener('focus', function(){
                 document.querySelector('.ui-select-exemplo2.ui-select').classList.add('select-active');
            }, true);
            selectAdvanced.addEventListener('blur', function(){
                setTimeout(function () {
                    document.querySelector('.ui-select-exemplo2.ui-select').classList.remove('select-active');
                }, 100);
            }, true);

            //zoom-lookup
            let inputSearchAdvanced = document.getElementById('input-zoom-lookup2');
            let searchOptionsAdvanced = document.querySelector('.zoom-lookup-exemplo2 .zoom-lookup-options');
            let searchArrayAdvanced = ['cancelado', 'em execução', 'finalizado', 'em aprovação', 'em revisão', 'lorem ipsum', 'ipsum lorem'];
            let inputSearch_val_advanced;
            let autocomplete_results_advanced;

            inputSearchAdvanced.addEventListener('focus', function(){
                $('.zoom-lookup-exemplo2 .zoom-lookup-wrapper').addClass('zoom-lookup-active');
            }, true);
            inputSearchAdvanced.addEventListener('blur', function(){
                $('.zoom-lookup-exemplo2 .zoom-lookup-wrapper').removeClass('zoom-lookup-active');
            }, true);

            function autocompleteAdvanced(val) {
              var searchArray_return = [];

              for (var i = 0; i < searchArrayAdvanced.length; i++) {
                if (val === searchArrayAdvanced[i].slice(0, val.length)) {
                  searchArray_return.push(searchArrayAdvanced[i]);
                }
              }

              return searchArray_return;
            }

            function handleActiveSearchAdvanced(){
                searchOptionsAdvanced.classList.remove('zoom-lookup-active');
            }
            function removeTagAdvanced(){
                $('.zoom-lookup-exemplo2 .ui-tag').on('click', '.remove-tag', function(){
                    $(this).closest('.ui-tag').remove();
                    if(!$('.zoom-lookup-exemplo2 .ui-tag').length){
                        $('.zoom-lookup-exemplo2 .zoom-lookup-wrapper').removeClass('has-tag');
                    }
                });
                $('.zoom-lookup-exemplo2 .remove-tags').on('click', function(){
                    $('.zoom-lookup-exemplo2 .ui-tag').remove();
                    $('.zoom-lookup-exemplo2 .zoom-lookup-wrapper').removeClass('has-tag');
                });
            }

            function handleTagsAdvanced(val) {
                $('.zoom-lookup-exemplo2 .zoom-lookup-wrapper').addClass('has-tag');
                $(inputSearchAdvanced).before("<span class='ui-tag'><a href='#'>"+ val +"</a><span class='icon-element remove-tag'><i class='fa fa-times'></i></span></span>");
                inputSearchAdvanced.value = '';
                removeTagAdvanced();
            }

            function bindEventsOnListAdvanced(){
                Array.from(searchOptionsAdvanced.querySelectorAll('li')).forEach(function(option){
                    option.addEventListener('click', function(e){
                        handleActiveSearchAdvanced();
                        let val = this.textContent;
                        handleTagsAdvanced(val);
                    });
                });
            }

            inputSearchAdvanced.onkeyup = function(e) {
                //has-tag
              inputSearch_val_advanced = this.value; // updates the variable on each ocurrence

              if (inputSearch_val_advanced.length > 0 && e.keyCode !== 27) {
                searchOptionsAdvanced.classList.add('zoom-lookup-active');
                var searchArray_to_show = [];

                autocomplete_results_advanced = document.getElementById("js-zoom-lookup-options2");
                autocomplete_results_advanced.innerHTML = '';
                searchArray_to_show = autocompleteAdvanced(inputSearch_val_advanced);

                for (var i = 0; i < searchArray_to_show.length; i++) {
                  autocomplete_results_advanced.innerHTML += '<li>' + searchArray_to_show[i] + '</li>';
                }
                if(!searchArray_to_show.length){
                    $('.zoom-lookup-exemplo2 .zoom-lookup-create').addClass('show-create');
                    $('.zoom-lookup-exemplo2 .create-zoom').text(inputSearch_val_advanced);
                    $('.zoom-lookup-exemplo2 .list-more').removeClass('show-list-more');
                }else{
                    $('.zoom-lookup-exemplo2 .zoom-lookup-create').removeClass('show-create');
                }
                if(searchArray_to_show.length > 5){
                    $('.zoom-lookup-exemplo2 .list-more').addClass('show-list-more');
                }else{
                    $('.zoom-lookup-exemplo2 .list-more').removeClass('show-list-more');
                }
                bindEventsOnListAdvanced();
              } else {
                inputSearchAdvanced.value = '';
                handleActiveSearchAdvanced();
                searchArray_to_show = [];
                autocomplete_results_advanced.innerHTML = '';
              }
            }
        //:

        //:exemplo search tag
            //select
            let selectAdvanced2 = document.querySelector('.ui-select-exemplo3.ui-select input');
            let selectOptionsAdvanced2 = document.querySelectorAll('.ui-select-exemplo3 .ui-select-options li');
            Array.from(selectOptionsAdvanced2).forEach(function(option){
                option.addEventListener('click', function(e) {
                    $('.advanced-search2 .zoom-lookup').removeClass('is-disabled');
                    $('.advanced-search2 .ui-button').removeClass('is-disable');
                    $('.advanced-search2 .zoom-lookup input').trigger('focus');
                    let val = e.target.textContent;
                    selectAdvanced2.value = val;
                });
            });
            selectAdvanced2.addEventListener('focus', function(){
                 document.querySelector('.ui-select-exemplo3.ui-select').classList.add('select-active');
            }, true);
            selectAdvanced2.addEventListener('blur', function(){
                setTimeout(function () {
                    document.querySelector('.ui-select-exemplo3.ui-select').classList.remove('select-active');
                }, 100);
            }, true);

            //zoom-lookup
            let inputSearchAdvanced2 = document.getElementById('input-zoom-lookup3');
            let searchOptionsAdvanced2 = document.querySelector('.zoom-lookup-exemplo3 .zoom-lookup-options');
            let searchArrayAdvanced2 = ['cancelado', 'em execução', 'finalizado', 'em aprovação', 'em revisão', 'lorem ipsum', 'ipsum lorem'];
            let inputSearch_val_advanced2;
            let autocomplete_results_advanced2;

            inputSearchAdvanced2.addEventListener('focus', function(){
                $('.input-zoom-lookup3 .zoom-lookup-wrapper').addClass('zoom-lookup-active');
            }, true);
            inputSearchAdvanced2.addEventListener('blur', function(){
                $('.input-zoom-lookup3 .zoom-lookup-wrapper').removeClass('zoom-lookup-active');
            }, true);

            function autocompleteAdvanced2(val) {
              var searchArray_return = [];

              for (var i = 0; i < searchArrayAdvanced2.length; i++) {
                if (val === searchArrayAdvanced2[i].slice(0, val.length)) {
                  searchArray_return.push(searchArrayAdvanced2[i]);
                }
              }

              return searchArray_return;
            }

            function handleActiveSearchAdvanced2(){
                searchOptionsAdvanced2.classList.remove('zoom-lookup-active');
            }
            function removeTagAdvanced2(){
                $('.zoom-lookup-exemplo3 .ui-tag').on('click', '.remove-tag', function(){
                    $(this).closest('.ui-tag').remove();
                    if(!$('.zoom-lookup-exemplo3 .ui-tag').length){
                        $('.zoom-lookup-exemplo3 .zoom-lookup-wrapper').removeClass('has-tag');
                        $('.advanced-search2 .ui-button').addClass('is-disable');
                    }
                });
                $('.zoom-lookup-exemplo3 .remove-tags').on('click', function(){
                    $('.zoom-lookup-exemplo3 .ui-tag').remove();
                    $('.zoom-lookup-exemplo3 .zoom-lookup-wrapper').removeClass('has-tag');
                    $('.advanced-search2 .ui-button').addClass('is-disable');
                });
            }

            function handleTagsAdvanced2(val) {
                $('.zoom-lookup-exemplo3 .zoom-lookup-wrapper').addClass('has-tag');
                $(inputSearchAdvanced2).before("<span class='ui-tag'><a href='#'>"+ val +"</a><span class='icon-element remove-tag'><i class='fa fa-times'></i></span></span>");
                inputSearchAdvanced2.value = '';
                removeTagAdvanced2();
            }

            function bindEventsOnListAdvanced2(){
                Array.from(searchOptionsAdvanced2.querySelectorAll('li')).forEach(function(option){
                    option.addEventListener('click', function(e){
                        handleActiveSearchAdvanced2();
                        let val = this.textContent;
                        handleTagsAdvanced2(val);
                    });
                });
            }

            inputSearchAdvanced2.onkeyup = function(e) {
                //has-tag
              inputSearch_val_advanced2 = this.value; // updates the variable on each ocurrence

              if (inputSearch_val_advanced2.length > 0 && e.keyCode !== 27) {
                searchOptionsAdvanced2.classList.add('zoom-lookup-active');
                var searchArray_to_show = [];

                autocomplete_results_advanced2 = document.getElementById("js-zoom-lookup-options3");
                autocomplete_results_advanced2.innerHTML = '';
                searchArray_to_show = autocompleteAdvanced2(inputSearch_val_advanced2);

                for (var i = 0; i < searchArray_to_show.length; i++) {
                  autocomplete_results_advanced2.innerHTML += '<li>' + searchArray_to_show[i] + '</li>';
                }
                if(!searchArray_to_show.length){
                    $('.input-zoom-lookup3 .zoom-lookup-create').addClass('show-create');
                    $('.input-zoom-lookup3 .create-zoom').text(inputSearch_val_advanced2);
                    $('.input-zoom-lookup3 .list-more').removeClass('show-list-more');
                }else{
                    $('.input-zoom-lookup3 .zoom-lookup-create').removeClass('show-create');
                }
                if(searchArray_to_show.length > 5){
                    $('.input-zoom-lookup3 .list-more').addClass('show-list-more');
                }else{
                    $('.input-zoom-lookup3 .list-more').removeClass('show-list-more');
                }
                bindEventsOnListAdvanced2();
              } else {
                inputSearchAdvanced2.value = '';
                handleActiveSearchAdvanced2();
                searchArray_to_show = [];
                autocomplete_results_advanced2.innerHTML = '';
              }
            }
            $('.ui-button-adicionar').on('click', function(){
                $('.advanced-search2 .ui-tags-add').prepend('<span class="ui-tag ui-tag-add"><p>Situação</p><a href="#">cancelado</a><span class="icon-element remove-tag"><i class="fa fa-times"></i></span></span>');
                $('.advanced-search2').addClass('advanced-search-active');
            });
        //:

        //:search icon
            let selectAdvanced3 = document.querySelector('.ui-select-exemplo4.ui-select input');
            let selectOptionsAdvanced3 = document.querySelectorAll('.ui-select-exemplo4 .ui-select-options li');
            Array.from(selectOptionsAdvanced3).forEach(function(option){
                option.addEventListener('click', function(e) {
                    $('.advanced-search3 .zoom-lookup').removeClass('is-disabled');
                    $('.advanced-search3 .ui-button').removeClass('is-disable');
                    $('.advanced-search3 .zoom-lookup input').trigger('focus');
                    let icon = $(this).find('span').attr('class');
                    $('.select-icon').find('span').attr('class', icon);
                });
            });
            selectAdvanced3.addEventListener('focus', function(){
                 document.querySelector('.ui-select-exemplo4.ui-select').classList.add('select-active');
            }, true);
            selectAdvanced3.addEventListener('blur', function(){
                setTimeout(function () {
                    document.querySelector('.ui-select-exemplo4.ui-select').classList.remove('select-active');
                }, 100);
            }, true);

            //zoom-lookup
            let inputSearchAdvanced3 = document.getElementById('input-zoom-lookup4');
            let searchOptionsAdvanced3 = document.querySelector('.zoom-lookup-exemplo4 .zoom-lookup-options');
            let searchArrayAdvanced3 = ['cancelado', 'em execução', 'finalizado', 'em aprovação', 'em revisão', 'lorem ipsum', 'ipsum lorem'];
            let inputSearch_val_advanced3;
            let autocomplete_results_advanced3;

            inputSearchAdvanced3.addEventListener('focus', function(){
                $('.input-zoom-lookup4 .zoom-lookup-wrapper').addClass('zoom-lookup-active');
            }, true);
            inputSearchAdvanced3.addEventListener('blur', function(){
                $('.input-zoom-lookup4 .zoom-lookup-wrapper').removeClass('zoom-lookup-active');
            }, true);

            function autocompleteAdvanced3(val) {
              var searchArray_return = [];

              for (var i = 0; i < searchArrayAdvanced3.length; i++) {
                if (val === searchArrayAdvanced3[i].slice(0, val.length)) {
                  searchArray_return.push(searchArrayAdvanced3[i]);
                }
              }

              return searchArray_return;
            }

            function handleActiveSearchAdvanced3(){
                searchOptionsAdvanced3.classList.remove('zoom-lookup-active');
            }
            function removeTagAdvanced3(){
                $('.zoom-lookup-exemplo4 .ui-tag').on('click', '.remove-tag', function(){
                    $(this).closest('.ui-tag').remove();
                    if(!$('.zoom-lookup-exemplo4 .ui-tag').length){
                        $('.zoom-lookup-exemplo4 .zoom-lookup-wrapper').removeClass('has-tag');
                        $('.advanced-search2 .ui-button').addClass('is-disable');
                    }
                });
                $('.zoom-lookup-exemplo4 .remove-tags').on('click', function(){
                    $('.zoom-lookup-exemplo4 .ui-tag').remove();
                    $('.zoom-lookup-exemplo4 .zoom-lookup-wrapper').removeClass('has-tag');
                    $('.advanced-search3 .ui-button').addClass('is-disable');
                });
            }

            function handleTagsAdvanced3(val) {
                $('.zoom-lookup-exemplo4 .zoom-lookup-wrapper').addClass('has-tag');
                $(inputSearchAdvanced3).before("<span class='ui-tag'><a href='#'>"+ val +"</a><span class='icon-element remove-tag'><i class='fa fa-times'></i></span></span>");
                inputSearchAdvanced3.value = '';
                removeTagAdvanced3();
            }

            function bindEventsOnListAdvanced3(){
                Array.from(searchOptionsAdvanced3.querySelectorAll('li')).forEach(function(option){
                    option.addEventListener('click', function(e){
                        handleActiveSearchAdvanced3();
                        let val = this.textContent;
                        handleTagsAdvanced3(val);
                    });
                });
            }

            inputSearchAdvanced3.onkeyup = function(e) {
                //has-tag
              inputSearch_val_advanced3 = this.value; // updates the variable on each ocurrence

              if (inputSearch_val_advanced3.length > 0 && e.keyCode !== 27) {
                searchOptionsAdvanced3.classList.add('zoom-lookup-active');
                var searchArray_to_show = [];

                autocomplete_results_advanced3 = document.getElementById("js-zoom-lookup-options4");
                autocomplete_results_advanced3.innerHTML = '';
                searchArray_to_show = autocompleteAdvanced3(inputSearch_val_advanced3);

                for (var i = 0; i < searchArray_to_show.length; i++) {
                  autocomplete_results_advanced3.innerHTML += '<li>' + searchArray_to_show[i] + '</li>';
                }
                if(!searchArray_to_show.length){
                    $('.input-zoom-lookup4 .zoom-lookup-create').addClass('show-create');
                    $('.input-zoom-lookup4 .create-zoom').text(inputSearch_val_advanced3);
                    $('.input-zoom-lookup4 .list-more').removeClass('show-list-more');
                }else{
                    $('.input-zoom-lookup4 .zoom-lookup-create').removeClass('show-create');
                }
                if(searchArray_to_show.length > 5){
                    $('.input-zoom-lookup4 .list-more').addClass('show-list-more');
                }else{
                    $('.input-zoom-lookup4 .list-more').removeClass('show-list-more');
                }
                bindEventsOnListAdvanced3();
              } else {
                  inputSearchAdvanced3.value = '';
                handleActiveSearchAdvanced3();
                searchArray_to_show = [];
                autocomplete_results_advanced3.innerHTML = '';
              }
            }
        //:

        //: text
        let selectAdvanced5 = document.querySelector('.ui-select-exemplo5.ui-select input');
        let selectOptionsAdvanced5 = document.querySelectorAll('.ui-select-exemplo5 .ui-select-options li');
        Array.from(selectOptionsAdvanced5).forEach(function(option){
            option.addEventListener('click', function(e) {
                $('.advanced-search-text .ui-input').removeClass('is-disabled');
                $('.advanced-search-text .ui-button').removeClass('is-disable');
                $('.advanced-search-text .ui-input:not(".ui-select") input').trigger('focus');
                let val = e.target.textContent;
                selectAdvanced5.value = val;
            });
        });
        selectAdvanced5.addEventListener('focus', function(){
             document.querySelector('.ui-select-exemplo5.ui-select').classList.add('select-active');
        }, true);
        selectAdvanced5.addEventListener('blur', function(){
            setTimeout(function () {
                document.querySelector('.ui-select-exemplo5.ui-select').classList.remove('select-active');
            }, 100);
        }, true);
        //:

        //: integer
        let selectAdvanced6 = document.querySelector('.ui-select-exemplo6.ui-select input');
        let selectOptionsAdvanced6 = document.querySelectorAll('.ui-select-exemplo6 .ui-select-options li');
        Array.from(selectOptionsAdvanced6).forEach(function(option){
            option.addEventListener('click', function(e) {
                $('.advanced-search-integer .ui-input').removeClass('is-disabled');
                $('.advanced-search-integer .ui-button').removeClass('is-disable');
                $('.advanced-search-integer .ui-input:not(".ui-select") input').trigger('focus');
                let val = e.target.textContent;
                selectAdvanced6.value = val;
            });
        });
        selectAdvanced6.addEventListener('focus', function(){
             document.querySelector('.ui-select-exemplo6.ui-select').classList.add('select-active');
        }, true);
        selectAdvanced6.addEventListener('blur', function(){
            setTimeout(function () {
                document.querySelector('.ui-select-exemplo6.ui-select').classList.remove('select-active');
            }, 100);
        }, true);
        //:
    //:

    // body event
    $('body').on('click', function(event) {
        if($('.zoom-lookup-options').hasClass('zoom-lookup-active')){
            $('.zoom-lookup-options').closest('.zoom-lookup-wrapper').find('input').val('');
            $('.zoom-lookup-options').removeClass('zoom-lookup-active');
        }
    });
})
