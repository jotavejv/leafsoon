'use strict';

var gulp = require('gulp');
var webpack = require('webpack-stream');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();
var run = require('gulp-run-command').default;
var fileinclude = require('gulp-file-include');
var gulpReplace = require('gulp-replace');
var argv = require('yargs').argv;
var fs=require('fs');

var autoprefixerOptions = {
  browsers: ['last 4 versions', 'safari >= 5', 'ie 11', 'opera >= 12.1', 'ios >= 6', 'android >= 4']
};

var fileincludeOptions = {
  prefix: '@',
  basepath: './includes',
  context: {}
}

gulp.task('sass', function() {
    return gulp.src('sass/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'compact'}).on('error', sass.logError))
    .pipe(sourcemaps.write())
    .pipe(autoprefixer(autoprefixerOptions))
    .pipe(gulp.dest('./dist/css'))
    .pipe(browserSync.stream({match: '**/*.css'}));
});

gulp.task('js', function() {
    return gulp.src('./js/main.js')
    .pipe(webpack( require('./webpack.config.js') ))
    .pipe(gulp.dest('dist/js'))
    .pipe(browserSync.stream({match: '**/*.js'}));
});

var htmlRootFiles = fs.readdirSync("./").filter(html => html.includes('.html'));
function handlePublic(){
    return Promise.all(
        htmlRootFiles.map( (file) => {
            new Promise ((resolve, reject) => {
                return gulp.src('./' + file)
                .on('error', reject)
                .pipe(fileinclude(fileincludeOptions))
                .pipe(gulp.dest('./dist'))
                .on('end', resolve)
            })
        })
    )
}

gulp.task('browser-sync',  function() {
    browserSync.init({
        server: {
            baseDir: "./dist"
        }
    });
    gulp.watch('sass/**/*.scss',['sass']);
    gulp.watch('js/**/*.js',['js']);
    gulp.watch(['./**/*.html', '!./dist', '!./dist/**'], function(e){
        handlePublic().then(function(){
            setTimeout(function () {
                browserSync.reload();
            }, 500);
        })
    });
});


var files = [
    'components/**/*.*',
    'img/**/*.*',
    'fonts/**/*.*'
];

gulp.task('icons', function() {
    return gulp.src('bower_components/font-awesome/fonts/**.*')
        .pipe(gulp.dest('./fonts'));
});

gulp.task('dist', function() {
  return gulp.src(files , { base: './' })
    .pipe(gulp.dest('dist'));
});

gulp.task('default', ['browser-sync', 'icons', 'dist'], function() { /**/ });
